// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDNtv9bpMz1lOamvtCg4uhk8R7wKLAikqI",
  authDomain: "shop24h-project-82590.firebaseapp.com",
  projectId: "shop24h-project-82590",
  storageBucket: "shop24h-project-82590.appspot.com",
  messagingSenderId: "110140710284",
  appId: "1:110140710284:web:a5c5035b375ff564380aa9",
  measurementId: "G-1T31BKJVRB"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);
export const storage = getStorage(app);
export default auth;