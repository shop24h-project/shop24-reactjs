import { combineReducers } from "redux";
import userLoginReducer from "./userLoginReducer";
import getProductReducer from "./getProductReducer";
import getCategoryReducer from "./getCategoryReducer";
import cartReducer from "./cartReducer";
import createOrderReducer from "./createOrderReducer";
import createOrderDetailReducer from "./createOrderDetailReducer";
const rootReducer = combineReducers({
    userLoginReducer,
    getProductReducer,
    getCategoryReducer,
    cartReducer,
    createOrderReducer,
    createOrderDetailReducer
});

export default rootReducer;
