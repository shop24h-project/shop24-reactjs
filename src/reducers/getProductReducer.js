import { GET_ALL_PRODUCT, GET_POPULAR_PRODUCT, GET_PRODUCT_BY_ID, CHANGE_PAGE_PAGINATION, FILTER_PRODUCTS_BY_PRICE} from "../constants/getProduct";

const limit = 2;

const initialState = {
    currentPage: 1,
    products: [],
    countAll: 0,
    noPage: 0,
    productInfo: "",
    priceMin: 49,
    priceMax: 360
}

export default function getProductReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_PRODUCT:
            return {
                ...state,
                countAll : action.data.length,
                products: action.data.slice((state.currentPage - 1) * limit, state.currentPage * limit),
                noPage: Math.ceil(action.data.length / limit),
                priceMin: action.priceMin,
                priceMax: action.priceMax
            }
            case GET_POPULAR_PRODUCT:
                return {
                    ...state,
                    products: action.data,
                }
        case GET_PRODUCT_BY_ID:
            return {
                ...state,
                productInfo: action.payload
            }
        case CHANGE_PAGE_PAGINATION:
            return {
                ...state,
                currentPage: action.payload
            }
            // case FILTER_PRODUCTS_BY_PRICE:
            //     return {
            //       ...state,
            //       products: action.data,
            //       priceMin: action.priceMin,
            //       priceMax: action.priceMax,
            //     };
        default:
            return state;
    }
}
