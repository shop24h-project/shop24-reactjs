import { USER_LOGIN } from "../constants/userLogin";

const initialState = {
  user: "",
}

const userLoginReducer = (state= initialState, action) => {
  switch(action.type){
    case USER_LOGIN:
            return {
                ...state,
                user: action.payload
            }
    default:
        return state;
  }
} 

export default userLoginReducer;
