import { GET_CATEGORY_LIMIT, GET_ALL_CATEGORY, GET_CATEGORY_BY_ID } from "../constants/getCategory";


const initialState = {
    categories: [],
    categoryInfo: "",
}

export default function getCategoryReducer(state = initialState, action) {
    switch (action.type) {
        case GET_CATEGORY_LIMIT:
            return {
                ...state,
                categories: action.data,
            }
        case GET_ALL_CATEGORY:
            return {
                ...state,
                categories: action.data,
            }
            case GET_CATEGORY_BY_ID:
                return {
                    ...state,
                    categoryInfo: action.payload,
                }
        default:
            return state;
    }
}
