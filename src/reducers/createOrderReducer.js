import { CREATE_ORDER, CREATE_ORDER_ERROR, CREATE_ORDER_PENDING } from "../constants/createOrder";

const initialState = {
  createOrder: {},
  pending: false,
  error: null
}

export default function createOrderReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_ORDER_PENDING:
      return {
        ...state,
        pending: true
      };
    case CREATE_ORDER:
      return {
        ...state,
        createOrder: action.payload
      }
    case CREATE_ORDER_ERROR:
      return {
        ...state,
        error: action.error,
        pending: false
      };
    default:
      return state;
  }
}
