import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import '@fortawesome/fontawesome-free/css/all.min.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Home from './pages/Home';
import Login from './pages/Login';
import Category from './pages/Category';
import ProductInfo from './pages/ProductInfo';
import Search from './pages/Search';
import Cart from './pages/Cart';
import Checkout from './pages/Checkout';
function App() {
  return (
    <BrowserRouter>
      <>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path="/login" element={<Login />}/>
            <Route path="/category/:categoryid" element={<Category/>}/>
            <Route path="/category" element={<Category/>}></Route>
            <Route path="/search/:name" element={<Search/>}/>
            <Route path="/cart" element={<Cart/>}/>
            <Route path="/checkout" element={<Checkout/>}/>
          <Route path="/products/:productid" element={<ProductInfo/>}/>
        </Routes>
      </>
    </BrowserRouter>
  );
}

export default App;

