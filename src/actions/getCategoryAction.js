import {
    GET_CATEGORY_LIMIT,
    GET_ALL_CATEGORY,
    GET_CATEGORY_BY_ID
    } from "../constants/getCategory";

    export const getCategoryLimit= () => async dispatch => {
  
      var requestOptions = {
          method: 'GET',
          redirect: 'follow',
          
      }
      try {
          const response = await fetch(
            `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types?limit=3`, requestOptions
            )
            const data = await response.json()
            return dispatch({
                type: GET_CATEGORY_LIMIT,
                data: data.productType
            });
      } catch (err) {
          console.log(err);
      }
    };
    
    export const getAllCategory = () => async dispatch => {
  
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        }
        try {
            const response = await fetch(
              `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types`, requestOptions
              )
              const data = await response.json()
              return dispatch({
                  type: GET_ALL_CATEGORY,
                  data: data.productType
              });
        } catch (err) {
            console.log(err);
        }
      };
      export const getCategoryById = (id) => async dispatch => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
            
        }
        try {
            const response = await fetch(
              `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types/${id}`, requestOptions
              )
            const data = await response.json();
            return dispatch({
                type: GET_CATEGORY_BY_ID,
                payload: data.productType
            });
        } catch (err) {
            // console.log(err)
        }
      }
      