import { CREATE_ORDER, CREATE_ORDER_ERROR, CREATE_ORDER_PENDING } from "../constants/createOrder";

export const createOrderActions = (formInput) => async dispatch => {
  var requestOptions = {
    method: 'POST',
    body: JSON.stringify(formInput),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  await dispatch({
    type: CREATE_ORDER_PENDING
  });

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/orders`, requestOptions
    );

    const data = await response.json();

    return dispatch({
      type: CREATE_ORDER,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: CREATE_ORDER_ERROR,
      error: err
    });
  }
};

