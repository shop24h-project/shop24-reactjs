import { CREATE_ORDER_DETAIL, CREATE_ORDER_DETAIL_ERROR, CREATE_ORDER_DETAIL_PENDING } from "../constants/createOrderDetail";

export const createOrderDetailAction = (orderId, orderDetail) => async dispatch => {
  var requestOptions = {
    method: 'POST',
    body: JSON.stringify(orderDetail),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  await dispatch({
    type: CREATE_ORDER_DETAIL_PENDING
  });

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/orders/${orderId}/order-details`, requestOptions
    );

    const data = await response.json();

    return dispatch({
      type: CREATE_ORDER_DETAIL,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: CREATE_ORDER_DETAIL_ERROR,
      error: err
    });
  }
};

