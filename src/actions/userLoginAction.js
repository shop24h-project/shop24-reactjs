import { USER_LOGIN } from "../constants/userLogin";

export function userLoginAction(value) {
  return {
    type: USER_LOGIN,
    payload: value
  }
}
