// import CarouselComponent from "../contenComponent/carousel/carouselComponent";
// import TittleComponent from "./tittle/tittleComponent";
import logo from '../../assets/img/core-img/logo.png';
import heart from '../../assets/img/core-img/heart.svg';
import userImg from '../../assets/img/core-img/user.svg';
import bag from '../../assets/img/core-img/bag.svg';
import bg6 from '../../assets/img/bg-img/bg-6.jpg';
import imgWelcome from '../../assets/img/bg-img/bg-1.jpg';
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { getAllCategory } from "../../actions/getCategoryAction";
import { getPopularProduct } from "../../actions/getProductAction";
import { signOut, onAuthStateChanged } from "firebase/auth";
import auth from "../../firebase";
import { userLoginAction } from "../../actions/userLoginAction"
import { useNavigate } from "react-router-dom";
function Header() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { categories } = useSelector((reduxData) => reduxData.getCategoryReducer);
    const { products } = useSelector((reduxData) => reduxData.getProductReducer);
    const { user } = useSelector((reduxData) => reduxData.userLoginReducer);
    const { cart } = useSelector((reduxData) => reduxData.cartReducer);
    const [width, setWidth] = React.useState(window.innerWidth);
    const [isActive, setIsActive] = useState(false);
    const [isActivePage, setIsActivePage] = useState(false);
    const [isActiveShop, setIsActiveShop] = useState(false);
    useEffect(() => {
        dispatch(getAllCategory());
        dispatch(getPopularProduct());
    }, []);
    var url_produck_detail = "/products/";
    if (products[0]) {
        url_produck_detail += products[0]._id;
    }
    const [name, setName] = React.useState("");
    const handleSubmit = event => {
        event.preventDefault();
        navigate('/search/' + name);
    };
    const logoutGoogle = () => {

        signOut(auth)
            .then(() => {
                dispatch(userLoginAction(null))
                navigate('/');
            })
            .catch((error) => {
                console.error(error);
            })
    }
    const loginHandler = () => {
        navigate('/login');
    }

    const handleToggle = event => {
        // 👇️ toggle isActive state on click
        setIsActive(current => !current);
    };

    const handleTogglePage = event => {
        // 👇️ toggle isActive state on click
        console.log("12")
        setIsActivePage(current => !current);
    };
    const handleToggleShop= event => {
        // 👇️ toggle isActive state on click
        console.log("12")
        setIsActiveShop(current => !current);
    };
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            if (result) {
                dispatch(userLoginAction(result))
            } else {
                dispatch(userLoginAction(null))
            }
        })
    }, [user])
    useEffect(() => {
        /* Inside of a "useEffect" hook add an event listener that updates
           the "width" state variable when the window size changes */
        window.addEventListener("resize", () => setWidth(window.innerWidth));

        /* passing an empty array as the dependencies of the effect will cause this
           effect to only run when the component mounts, and not each time it updates.
           We only want the listener to be added once */
    }, []);

    var pages = [{ name: "Home", url: "/" }, { name: "shop", url: "/category" }, { name: "Product Details", url: url_produck_detail },
    { name: "Checkout", url: "checkout.html" }, { name: "Blog", url: "blog.html" }, { name: "Single Blog", url: "single-blog.html" },
    { name: "Regular Page", url: "regular-page.html" }, { name: "Contact", url: "contact.html" }]
    return (
        <>
            <header className="header_area">
                <div className={`classy-nav-container d-flex align-items-center justify-content-between ${width < 576 ? "breakpoint-on" : "breakpoint-off"}`} >
                    {/* Classy Menu */}
                    <nav className="classy-navbar" id="essenceNav">
                        {/* <!-- Logo --> */}
                        <a className="nav-brand" href="/"><img src={logo} alt="" /></a>
                        {/* <!-- Navbar Toggler --> */}
                        <div className="classy-navbar-toggler">
                            <span className={isActive ? "navbarToggler active" : "navbarToggler"} onClick={handleToggle}><span></span><span></span><span></span></span>
                        </div>
                        {/* <!-- Menu --> */}
                        <div className={isActive ? "classy-menu menu-on" : "classy-menu"}>
                            {/* <!-- close btn --> */}
                            <div className="classycloseIcon" onClick={handleToggle}>
                                <div className="cross-wrap" ><span className="top"></span><span className="bottom"></span></div>
                            </div>
                            {/* <!-- Nav Start --> */}
                            <div className="classynav">
                                <ul>
                                    <li className={`megamenu-item ${isActiveShop ? "active" : ""}`} onClick={handleToggleShop}><a href="#">Shop</a>
                                        <div className="megamenu">
                                            <ul className="single-mega cn-col-4">
                                                {categories.map((category, index) => (

                                                    <li key={index}><a href="shop.html">{category.name}</a></li>

                                                ))}
                                            </ul>
                                            <div className="single-mega cn-col-4">
                                                <img src={bg6} alt="" />
                                            </div>
                                        </div>
                                        <span className="dd-arrow"></span>
                                        <span className="dd-trigger"></span>
                                    </li>
                                    <li className={`cn-dropdown-item has-down pr12 ${isActivePage ? "active" : ""}`} ><a href="#">Pages</a>
                                        <ul className="dropdown">
                                            {pages.map((page, index) => (

                                                <li key={index}><a href={page.url}>{page.name}</a></li>

                                            ))}
                                        </ul>
                                        <span className="dd-trigger" onClick={handleTogglePage}></span>
                                        <span className="dd-arrow"></span>
                                    </li>
                                    <li><a href="blog.html">Blog</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </div>
                            {/* <!-- Nav End --> */}
                        </div>
                    </nav>

                    {/* <!-- Header Meta Data --> */}
                    <div className="header-meta d-flex clearfix justify-content-end">
                        {/* <!-- Search Area --> */}
                        <div className="search-area">
                            <form action="#" onSubmit={handleSubmit}>
                                <input type="search" onChange={(e) => setName(e.target.value.toLowerCase())} name="search" id="headerSearch" placeholder="Type for search" />
                                <button type="submit"  ><i className="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                        </div>
                        {/* <!-- Favourite Area --> */}
                        <div className="favourite-area">
                            <a href="#"><img src={heart} alt="" /></a>
                        </div>
                        {/* <!-- User Login Info --> */}
                        <div className="user-login-info">
                            {/* <a href="#"><img src={user} alt="" /> */}
                            {user ?
                                <a href="#">
                                    <img referrerPolicy="no-referrer" src={user.photoURL} alt="" />
                                    <ul className="dropdown-user">
                                        <li>{user.displayName}</li>
                                        <li onClick={logoutGoogle}>logout</li>
                                    </ul>
                                </a>
                                :
                                <a href="#">
                                    <img src={userImg} alt="" />
                                    <ul className="dropdown-user">
                                        <li onClick={loginHandler}>Login</li>
                                    </ul>
                                </a>
                            }
                        </div>
                        {/* <!-- Cart Area --> */}
                        <div className="cart-area">
                            <a href="/cart" id="essenceCartBtn"><img src={bag} alt="" /> <span>{cart.length}</span></a>
                        </div>
                    </div>

                </div>
            </header>

        </>
    )
}
export default Header;
