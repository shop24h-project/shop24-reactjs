// import CarouselComponent from "../contenComponent/carousel/carouselComponent";
// import TittleComponent from "./tittle/tittleComponent";
import React, { useEffect } from 'react';
import breadcumb from '../../assets/img/bg-img/breadcumb.jpg';
import { getCategoryById } from "../../actions/getCategoryAction";
import { useDispatch, useSelector } from "react-redux";

function BreadCrumbCategory({categoryid, name}) {
    const dispatch = useDispatch();
    console.log(categoryid);
    useEffect(() => {
        dispatch(getCategoryById(categoryid));
    }, []);
    const { categoryInfo } = useSelector(reduxData => reduxData.getCategoryReducer);
    // console.log("categoryid", categoryid);
    return (
        <>
            <div className="breadcumb_area bg-img" style={{ backgroundImage: `url(${breadcumb})` }}>
                <div className="container h-100">
                    <div className="row h-100 align-items-center">
                        <div className="col-12">
                            <div className="page-title text-center">
                                <h2>{ categoryInfo ? categoryInfo.name : "All products"}</h2>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default BreadCrumbCategory;
