// import CarouselComponent from "../contenComponent/carousel/carouselComponent";
// import TittleComponent from "./tittle/tittleComponent";

// import ProductCard from '../ProductCard/ProductCard';
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { getCategoryLimit } from "../../actions/getCategoryAction";
import  CategoryCard  from "./CategoryCard";
import bg5 from '../../assets/img/bg-img/bg-5.jpg';
function TopCategory() {
    const dispatch = useDispatch();
    const { categories } = useSelector((reduxData) => reduxData.getCategoryReducer);
    useEffect(() => {
        dispatch(getCategoryLimit());
    }, []);
    return (
        <>
            <div className="top_catagory_area section-padding-80 clearfix">
                <div className="container">
                    <div className="row justify-content-center">
                        {categories.map((category, index) => (
                            <div key = {index} className="col-12 col-sm-6 col-md-4" >
                                <CategoryCard  category={category} />
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            {/*     
    <!-- ##### CTA Area Start ##### --> */}
            <div className="cta-area">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="cta-content bg-img background-overlay" style={{ backgroundImage: `url(${bg5})` }}>
                                <div className="h-100 d-flex align-items-center justify-content-end">
                                    <div className="cta--text">
                                        <h6>-60%</h6>
                                        <h2>Global Sale</h2>
                                        <a href="#" className="btn essence-btn">Buy Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <!-- ##### CTA Area End ##### --> */}
        </>
    )
}
export default TopCategory;
