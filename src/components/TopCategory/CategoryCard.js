

function CategoryCard({ category }) {
   
    return (
        <>
            <div className="single_catagory_area d-flex align-items-center justify-content-center bg-img" style={{ backgroundImage: `url(${category.imageUrl})` }}>
                                <div className="catagory-content">
                                    <a href={ `/category/${category._id}` }>{category.name}</a>
                                </div>
                            </div>
        </>
    )
}
export default CategoryCard