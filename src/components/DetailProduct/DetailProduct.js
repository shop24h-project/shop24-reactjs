// import CarouselComponent from "../contenComponent/carousel/carouselComponent";
// import TittleComponent from "./tittle/tittleComponent";/
import React, { useEffect, useState, useRef } from 'react';
import { useParams } from "react-router-dom";
import { getProductById } from "../../actions/getProductAction";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from '../../actions/cartAction';
import {useNavigate } from "react-router-dom";
import Slider from 'react-slick';
function DetailProduct() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { user } = useSelector((reduxData) => reduxData.userLoginReducer);
    const { cart } = useSelector((reduxData) => reduxData.cartReducer);
    const { productid } = useParams();
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };
    useEffect(() => {
        dispatch(getProductById(productid));
    }, [productid]);

    const { productInfo } = useSelector(reduxData => reduxData.getProductReducer);
    const addCartHandler = () => {
        if(!user){
            navigate('/login');
        }
        else{
            dispatch(addToCart(productInfo));
        }
    }
    function numberWithCommas(x) {
        x = x.toString();
        var pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x))
            x = x.replace(pattern, "$1.$2");
        return x;
    }
    return (
        <>
            <section className="single_product_details_area d-flex align-items-center">
                {/* <!-- Single Product Thumb --> */}
                <div className="single_product_thumb clearfix">
                    {productInfo ?
                        <Slider {...settings}>
                            <img src={productInfo.imageUrl} alt="" />
                            <img src={productInfo.subImage} alt="" />
                        </Slider>
                        : null
                    }
                </div>

                {/* <!-- Single Product Description --> */}
                {productInfo ?
                <div className="single_product_desc clearfix">
                    {/* <span>mango</span> */}
                    <a href="cart.html">
                        <h2>{ productInfo.name }</h2>
                    </a>
                    <p className="product-price"><span className="old-price">${numberWithCommas(productInfo.buyPrice) }</span> ${numberWithCommas(productInfo.promotionPrice)}</p>
                    <p className="product-desc">{productInfo.description}</p>

                    {/* <!-- Form --> */}
                    <form className="cart-form clearfix" method="post">
                        {/* <!-- Select Box --> */}
                        <div className="select-box d-flex mt-50 mb-30">
                            <div className="nice-select mr-5" tabIndex="0"><span className="current">Size: XL</span><ul className="list"><li data-value="value" className="option selected focus">Size: XL</li><li data-value="value" className="option">Size: X</li><li data-value="value" className="option">Size: M</li><li data-value="value" className="option">Size: S</li></ul></div>
                            <div className="nice-select" tabIndex="0"><span className="current">Color: Black</span><ul className="list"><li data-value="value" className="option selected">Color: Black</li><li data-value="value" className="option">Color: White</li><li data-value="value" className="option">Color: Red</li><li data-value="value" className="option">Color: Purple</li></ul></div>
                        </div>
                        {/* <!-- Cart & Favourite Box --> */}
                        <div className="cart-fav-box d-flex align-items-center">
                            {/* <!-- Cart --> */}
                            <div className="add-to-cart-btn">
                            <a onClick={addCartHandler} className="btn essence-btn">Add to Cart</a>
                        </div>
                            {/* <!-- Favourite --> */}
                            <div className="product-favourite ml-4">
                                <a href="#" className="favme fa fa-heart"></a>
                            </div>
                        </div>
                    </form>
                </div>
                : null
                }
            </section>
        </>
    )
}
export default DetailProduct;
