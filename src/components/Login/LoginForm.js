import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { userLoginAction } from "../../actions/userLoginAction";
import { useEffect } from "react";
import { GoogleAuthProvider, signInWithPopup, signOut, onAuthStateChanged } from "firebase/auth";

import auth from "../../firebase";
const provider = new GoogleAuthProvider();
function LoginForm() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { user } = useSelector(reduxData => reduxData.userLoginReducer);

    async function logInGoogle() {
        console.log("1222");
        await signInWithPopup(auth, provider).then(
            async (result) => {
                //3 - pick the result and store the token
                const token = await auth?.currentUser?.getIdToken(true);
                console.log(result);
                //4 - check if have token in the current user
                if (token) {
                    //5 - put the token at localStorage (We'll use this to make requests)
                    localStorage.setItem("@token", token);
                    //6 - navigate user to the book list
                    dispatch(userLoginAction(result));
                    console.log(token)
                    navigate('/');
                }
            })
            .catch((error) => {
                console.log(error);
            })
    }
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            if (result) {
                dispatch(userLoginAction(result));
            }
            else {
                dispatch(userLoginAction(null));
            }
        })
    }, [user])

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <div className="card border-0 shadow rounded-3 my-5">
                            <div className="card-body p-4 p-sm-5">
                                <h5 className="card-title text-center mb-5 fw-light fs-5">Sign In</h5>
                                <div className="login-form">
                                    <div className="d-grid mb-2">
                                        <button onClick={logInGoogle} className="w-100 btn btn-google btn-login text-uppercase fw-bold">
                                            <i className="fab fa-google me-2"></i> Sign in with Google
                                        </button>
                                    </div>
                                    <div className="seperator"><b>or</b></div>
                                    <div className="form-floating mb-3">
                                        <input type="email" className="form-control" id="floatingInput" placeholder="name@example.com" />
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input type="password" className="form-control" id="floatingPassword" placeholder="Password" />
                                    </div>

                                    <div className="form-check mb-3">
                                        <input className="form-check-input" type="checkbox" value="" id="rememberPasswordCheck" />
                                        <label className="form-check-label">
                                            Remember password
                                        </label>
                                    </div>
                                    <div className="d-grid">
                                        <button className="w-100 btn btn-primary btn-login text-uppercase fw-bold" type="submit">Sign
                                            in</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default LoginForm;
