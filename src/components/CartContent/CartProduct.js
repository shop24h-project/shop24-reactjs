// import CarouselComponent from "../contenComponent/carousel/carouselComponent";
// import TittleComponent from "./tittle/tittleComponent";
import React, { useEffect, useState } from "react";
import { deleteFromCart } from '../../actions/cartAction';
// import { getCategoryById } from "../../actions/getCategoryAction";
import { useDispatch, useSelector } from "react-redux";

function CartProduct({item, addTotalProp, decreaseProp, cart, index}) {

    const [quantity, setQuantity] = useState( cart[index].count);
    const [totalProduct, setTotalProduct] = useState(quantity*item.promotionPrice);
	const dispatch = useDispatch();

        const addProduct = () => {
            setQuantity(quantity + 1);
            cart[index].count++;
            localStorage.setItem('cart', JSON.stringify(cart));
            setTotalProduct(totalProduct + item.promotionPrice);
            addTotalProp(item.promotionPrice);
            console.log(cart);
        }
        const decreaseProduct = () => {
            if(quantity > 1){
                setQuantity(quantity - 1);
                cart[index].count--;
                localStorage.setItem('cart', JSON.stringify(cart));
                setTotalProduct(totalProduct - item.promotionPrice);
                decreaseProp(item.promotionPrice);
                console.log(cart);
            }   
        }
        const deleteProduct = () => {
          dispatch(deleteFromCart(item));
          decreaseProp(totalProduct);
        }
        function numberWithCommas(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1.$2");
            return x;
        }
    return (
        <>

            <tr>
                <td className="shoping__cart__item">
                    <img src={item.imageUrl.imageUrl1} alt="" />
                    <h5>{item.name}</h5>
                </td>
                <td className="shoping__cart__price">
                    ${numberWithCommas(item.promotionPrice)}
                </td>
                <td className="shoping__cart__quantity">
                    <div className="quantity">
                        <div className="pro-qty"><span className="dec qtybtn" onClick={decreaseProduct}>-</span>
                            <input type="text" value={quantity} readOnly/>
                            <span className="inc qtybtn" onClick={addProduct}>+</span></div>
                    </div>
                </td>
                <td className="shoping__cart__total">
                    ${totalProduct}
                </td>
                <td className="shoping__cart__item__close">
                <i className="fa fa-trash" aria-hidden="true" onClick={deleteProduct}></i>
                </td>
            </tr>


        </>
    )
}
export default CartProduct;
