// import CarouselComponent from "../contenComponent/carousel/carouselComponent";
// import TittleComponent from "./tittle/tittleComponent";
import breadcumb from '../../assets/img/bg-img/breadcumb.jpg';
import React, { useEffect, useState } from "react";
// import { getCategoryById } from "../../actions/getCategoryAction";
import {useSelector } from "react-redux";
import CartProduct from './CartProduct';
function CartContent() {
    const { cart } = useSelector((reduxData) => reduxData.cartReducer);
        const defaultTotal = cart.reduce(
            (previousValue, currentValue) => previousValue + currentValue.promotionPrice * currentValue.count,
            0,
          );
        const [cost, setCost] = useState(defaultTotal);

        const addCost = (value) => {
          setCost(cost + value);
        }
        const decreaseCost = (value) => {
            setCost(cost - value);
          }
          function numberWithCommas(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1.$2");
            return x;
        }
        console.log(cart);
    return (
        <>
            <div className="breadcumb_area bg-img" style={{ backgroundImage: `url(${breadcumb})` }}>
                <div className="container h-100">
                    <div className="row h-100 align-items-center">
                        <div className="col-12">
                            <div className="page-title text-center">
                                <h2>Shopping Cart</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section className="shoping-cart spad">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="shoping__cart__table">
                                <table>
                                    <thead>
                                        <tr>
                                            <th className="shoping__product">Products</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {cart.map((item, index) => (
                                            <CartProduct index = {index} cart={cart} key = {index} item = {item} addTotalProp={addCost} decreaseProp={decreaseCost} />
                                        ))}

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="shoping__cart__btns">
                                <a href="/category" className="primary-btn cart-btn">CONTINUE SHOPPING</a>
                            </div>
                        </div>
                        {/* <div className="col-lg-6">
                            <div className="shoping__continue">
                                <div className="shoping__discount">
                                    <h5>Discount Codes</h5>
                                    <form action="#">
                                        <input type="text" placeholder="Enter your coupon code" />
                                        <button type="submit" className="site-btn">APPLY COUPON</button>
                                    </form>
                                </div>
                            </div>
                        </div> */}
                        <div className="col-lg-6">
                            <div className="shoping__checkout">
                                <h5>Cart Total</h5>
                                <ul>
                                    <li>Subtotal <span>${numberWithCommas(cost)}</span></li>
                                    <li>Shipping <span>Free</span></li>
                                    <li>Total <span>${numberWithCommas(cost)}</span></li>
                                </ul>
                                <a href="/checkout" className="primary-btn">PROCEED TO CHECKOUT</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default CartContent;
