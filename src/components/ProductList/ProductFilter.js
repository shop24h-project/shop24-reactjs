import React, { useEffect, useState } from "react";
import ReactDOM from 'react-dom';
import { Typography, Slider } from '@mui/material';
import { getAllProduct } from "../../actions/getProductAction";
import { getAllCategory} from "../../actions/getCategoryAction";
import { useDispatch, useSelector } from "react-redux";
function ProductFilter({categoryid, name}) {
    const dispatch = useDispatch();
    const [value, setValue] = React.useState([49, 360]);
    const { products,  currentPage} = useSelector((reduxData) => reduxData.getProductReducer);
    const { categories } = useSelector((reduxData) => reduxData.getCategoryReducer);

    // Changing State when volume increases/decreases
    const rangeSelector = (event, newValue) => {
        dispatch(getAllProduct(newValue, categoryid, name));
        setValue(newValue);
    };
    useEffect(() => {
        dispatch(getAllProduct(value, categoryid, name));
        dispatch(getAllCategory());
        console.log("useEffect");
    }, [currentPage, name]);
    console.log("auth",  localStorage.getItem("@token"))
    return (

        <>
            <div className="shop_sidebar_area">
                {/* <!-- ##### Single Widget ##### --> */}
                <div className="widget catagory mb-50">
                    {/* <!-- Widget Title --> */}
                    <h6 className="widget-title mb-30">Catagories</h6>

                    {/* <!--  Catagories  --> */}
                    <div className="catagories-menu">
                        <ul id="menu-content2" className="menu-content collapse show">
                            {/* <!-- Single Item --> */}
                            <li data-toggle="collapse" data-target="#clothing">
                                <ul className="sub-menu collapse show" id="clothing">
                                {categories.map((category, index) => (
                                        <li key={index}><a href={`/category/${category._id}`}>{category.name}</a></li>
                                    ))}
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>

                {/* <!-- ##### Single Widget ##### --> */}
                <div className="widget price mb-50">
                    {/* <!-- Widget Title --> */}
                    <h6 className="widget-title mb-30">Filter by</h6>
                    {/* <!-- Widget Title 2 --> */}
                    <p className="widget-title2 mb-30">Price</p>

                    <div className="widget-desc">
                        <div className="slider-range">
                            <Slider
                                value={value}
                                min={49}
                                max={360}
                                onChange={rangeSelector}
                            />
                        </div>
                        <div className="range-price">Range: ${value[0]}  - ${value[1]} </div>
                    </div>
                </div>

              
            </div>
        </>
    )
}
export default ProductFilter