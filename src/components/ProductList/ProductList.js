// import CarouselComponent from "../contenComponent/carousel/carouselComponent";
// import TittleComponent from "./tittle/tittleComponent";
import { Grid, Pagination } from "@mui/material";
import ProductCard from '../ProductCard/ProductCard';
import ProductFilter from './ProductFilter';
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect } from "react";
import { changePagePagination, getAllProduct } from "../../actions/getProductAction";

function ProductList({categoryid, name}) {

    const dispatch = useDispatch();
    const { countAll, currentPage, products, noPage, priceMin, priceMax } = useSelector((reduxData) => reduxData.getProductReducer);
    // useEffect(() => {
    //     dispatch(getAllProduct([priceMin, priceMax], categoryid));
    // }, []);
    const onChangePagination = (event, value) => {
        dispatch(changePagePagination(value));
      }
    
    return (
        <>
            <section className="shop_grid_area section-padding-80">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-4 col-lg-3">
                           <ProductFilter categoryid={categoryid} name = {name} currentPage={currentPage} />
                        </div>

                        <div className="col-12 col-md-8 col-lg-9">
                            <div className="shop_grid_product_area">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="product-topbar d-flex align-items-center justify-content-between">
                                            {/* <!-- Total Products --> */}
                                            <div className="total-products">
                                                <p><span>{countAll}</span> products found</p>
                                            </div>
                                            {/* <!-- Sorting --> */}
                                            {/* <div className="product-sorting d-flex">
                                                <p>Sort by:</p>
                                                <form action="#" method="get">
                                                    <select name="select" id="sortByselect">
                                                        <option value="value">Highest Rated</option>
                                                        <option value="value">Newest</option>
                                                        <option value="value">Price: $$ - $</option>
                                                        <option value="value">Price: $ - $$</option>
                                                    </select>
                                                    <input type="submit" className="d-none" value="" />
                                                </form>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    {products.map((product, index) => (
                                        <div key={index} className="col-12 col-sm-6 col-lg-4"> 
                                        <ProductCard product={product} />
                                        </div>
                                    ))}
                                </div>
                            </div>
                            {/* <!-- Pagination --> */}
                            <Grid item xs={12} md={12} sm={12} lg={12} >
                                <Grid container justifyContent={{ xs: "center", sm: "flex-end" }} >
                                    <Pagination
                                        count={noPage}
                                        defaultPage={1}
                                        onChange={onChangePagination}
                                        component="div"
                                    />
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default ProductList;
