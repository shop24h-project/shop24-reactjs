function ProductCard({ product }) {
    function numberWithCommas(x) {
        x = x.toString();
        var pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x))
            x = x.replace(pattern, "$1.$2");
        return x;
    }
    return (
        <>

            {/* <!-- Single Product --> */}
            <div className="col-12 col-sm-6 col-lg-4">
                <div className="single-product-wrapper">
                    {/* <!-- Product Image --> */}
                    <div className="product-img">
                        <img src={product.imageUrl} alt="" />
                        {/* <!-- Hover Thumb --> */}
                        <img className="hover-img" src={product.subImage} alt="" />

                        {/* <!-- Product Badge --> */}
                        <div className="product-badge offer-badge">
                            <span>-30%</span>
                        </div>
                        {/* <!-- Favourite --> */}
                        <div className="product-favourite">
                            <a href="#" className="favme fa fa-heart"></a>
                        </div>
                    </div>

                    {/* <!-- Product Description --> */}
                    <div className="product-description">
                        {/* <span>topshop</span> */}
                        <a href="single-product-details.html">
                            <h6>{product.name}</h6>
                        </a>
                        <p className="product-price"><span className="old-price">${numberWithCommas(product.buyPrice)}</span> ${numberWithCommas(product.promotionPrice)}</p>

                        {/* <!-- Hover Content --> */}
                        <div className="hover-content">
                            {/* <!-- Add to Cart --> */}
                            <div className="add-to-cart-btn">
                                <a href="#" className="btn essence-btn">Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default ProductCard