import { useDispatch, useSelector } from "react-redux";
import breadcumb from '../../assets/img/bg-img/breadcumb.jpg';
import React, { useEffect, useInsertionEffect, useState } from "react";
import { Alert, Select, Slider, Menu, MenuItem, Snackbar } from "@mui/material";
import { createOrderActions } from '../../actions/createOrderActions';
import { createOrderDetailAction } from '../../actions/createOrderDetailAction';
function CartContent() {
    const dispatch = useDispatch();
    const cart = localStorage.getItem('cart')
        ? JSON.parse(localStorage.getItem('cart'))
        : [];
    function numberWithCommas(x) {
        x = x.toString();
        var pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x))
            x = x.replace(pattern, "$1.$2");
        return x;
    }
    const totalPrice = cart.reduce(
        (previousValue, currentValue) => previousValue + currentValue.promotionPrice * currentValue.count,
        0,
    );

    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState();
    const [city, setCity] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [country, setCountry] = useState("vn");
    const [note, setNote] = useState("");
    const onFullNameChange = (event) => {
        setFullName(event.target.value);
    }
    const onCountryChange = (event) => {
        setCountry(event.target.value);
    }

    const onAddressChange = (event) => {
        setAddress(event.target.value);
    }
    const onCityChange = (event) => {
        setCity(event.target.value);
    }
    const onPhoneChange = (event) => {
        setPhone(event.target.value);
    }
    const onEmailChange = (event) => {
        setEmail(event.target.value);
    }
    const onNoteChange = (event) => {
        setNote(event.target.value);
    }
    const [openAlert, setOpenAlert] = useState(false);
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")


    const handleCloseAlert = () => {
        setOpenAlert(false);
    }
 

    const { error, createOrder } = useSelector((reduxData) => reduxData.createOrderReducer);
    const validateNewOrderData = (paramNewOrder) => {
        if (paramNewOrder.fullName === "") {
            setOpenAlert(true);
            setStatusModal("error");
            setNoidungAlertValid("Full name is required");
            return false;
        } 
        if (paramNewOrder.country === "") {
            setOpenAlert(true);
            setStatusModal("error");
            setNoidungAlertValid("Country is required");
            return false;
        } 
        if (paramNewOrder.address === "") {
            setOpenAlert(true);
            setStatusModal("error");
            setNoidungAlertValid("Address is required");
            return false;
        } 
        if (paramNewOrder.country === "") {
            setOpenAlert(true);
            setStatusModal("error");
            setNoidungAlertValid("Country is required");
            return false;
        } 
        if (paramNewOrder.city === "") {
            setOpenAlert(true);
            setStatusModal("error");
            setNoidungAlertValid("City is required");
            return false;
        } 
        if (paramNewOrder.phone === "") {
            setOpenAlert(true);
            setStatusModal("error");
            setNoidungAlertValid("Phone is required");
            return false;
        } 
        var vRegexStr = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!vRegexStr.test(paramNewOrder.email)) {
          setOpenAlert(true);
          setStatusModal("error")
          setNoidungAlertValid("Email không hợp lệ!");
          return false;
        }
        else {
            return true;
        }
    }
    let today = new Date();

    // let date =
    //   today.getDate() +
    //   "/" +
    //   parseInt(today.getMonth() + 1) +
    //   "/" +
    //   today.getFullYear();
  
    const createOrderClick = () => {
        var newOrder = {
            fullName: fullName,
            country: country,
            address: address,
            city: city,
            phone: phone,
            email: email,
            orderDate: today,
            requiredDate: today,
            shippedDate: today,
            note: note,
            cost: totalPrice,
            orderDetail: cart
        }
        var validateData = validateNewOrderData(newOrder);
        if (validateData) {
            dispatch(createOrderActions(newOrder));
        console.log(newOrder);
         if(!error) {
             setOpenAlert(true);
             setStatusModal("success");
             setNoidungAlertValid("Dữ liệu thêm thành công!");
            //  for (var bI = 0; bI < cart.length; bI++) {
            //     const orderDetailObject = {
            //         product: cart[bI]._id,
            //         quantity: cart[bI].count,
            //     };
            //  dispatch(createOrderDetailAction(orderId, orderDetailObject));
         
          
         } else {
             setOpenAlert(true);
             setStatusModal("error")
             setNoidungAlertValid("Dữ liệu thêm thất bại!")
             console.log(error);
         }
        }
        
    }
    return (
        <>
            <div className="breadcumb_area bg-img" style={{ backgroundImage: `url(${breadcumb})` }}>
                <div className="container h-100">
                    <div className="row h-100 align-items-center">
                        <div className="col-12">
                            <div className="page-title text-center">
                                <h2>Checkout</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="checkout_area section-padding-80">
                <div className="container">
                    <div className="row">

                        <div className="col-12 col-md-6">
                            <div className="checkout_details_area mt-50 clearfix">

                                <div className="cart-page-heading mb-30">
                                    <h5>Billing Address</h5>
                                </div>

                                <form action="#" method="post">
                                    <div className="row">
                                        <div className="col-12 mb-3">
                                            <label>Full Name <span>*</span></label>
                                            <input type="text" className="form-control" id="full_name" value={fullName} required onChange={onFullNameChange} />
                                        </div>
                                        <div className="col-12 mb-3">
                                            <label>Country <span>*</span></label>
                                            <Select
                                                MenuProps={{
                                                    disableScrollLock: true,
                                                }}
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={country}
                                                onChange={onCountryChange}
                                                style={{ width: "100%" }}
                                                disableScrollLock={true}
                                            >  
                                                 <MenuItem value="vn">Viet Nam</MenuItem>
                                                <MenuItem value="usa">United States</MenuItem>
                                                <MenuItem value="uk">United Kingdom</MenuItem>
                                                <MenuItem value="ger">Germany</MenuItem>
                                                <MenuItem value="fra">France</MenuItem>
                                                <MenuItem value="ind">India</MenuItem>
                                                <MenuItem value="aus">Australia</MenuItem>
                                                <MenuItem value="bra">Brazil</MenuItem>
                                                <MenuItem value="cana">Canada</MenuItem>
                                            </Select>
                                        </div>
                                        <div className="col-12 mb-3">
                                            <label>Address <span>*</span></label>
                                            <input type="text" className="form-control mb-3" id="street_address" value={address} onChange={onAddressChange} />
                                        </div>
                                        <div className="col-12 mb-3">
                                            <label>Town/City <span>*</span></label>
                                            <input type="text" className="form-control" id="city" value={city} onChange={onCityChange} />
                                        </div>
                                        <div className="col-12 mb-3">
                                            <label>Phone No <span>*</span></label>
                                            <input type="number" className="form-control" id="phone_number" min="0" value={phone} onChange={onPhoneChange} />
                                        </div>
                                        <div className="col-12 mb-4">
                                            <label>Email Address <span>*</span></label>
                                            <input type="email" className="form-control" id="email_address" value={email} onChange={onEmailChange} />
                                        </div>
                                        <div className="col-12 mb-4">
                                            <label>Note <span>*</span></label>
                                            <textarea class="form-control" rows="4" value={note} onChange={onNoteChange}></textarea>
                                        </div>
                                     

                                    </div>
                                </form>
                            </div>
                        </div>

                        <div className="col-12 col-md-6 col-lg-5 ml-lg-auto">
                            <div className="order-details-confirmation">
                                <div className="cart-page-heading">
                                    <h5>Your Order</h5>
                                    <p>The Details</p>
                                </div>

                                <ul className="order-details-form mb-4">
                                    <li><span>Product</span> <span>Total</span></li>
                                    {cart.map((item, index) => (
                                        <li key={index}><span>{item.name}</span> <span>${numberWithCommas(item.promotionPrice * item.count)}</span></li>
                                    ))}
                                    <li><span>Subtotal</span> <span>{totalPrice}</span></li>
                                    <li><span>Shipping</span> <span>Free</span></li>
                                    <li><span>Total</span> <span>{totalPrice}</span></li>
                                </ul>
                                <a onClick={createOrderClick} className="btn essence-btn">Place Order</a>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} style={{ bottom: "0px"}}>
                    <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                        {noidungAlertValid}
                    </Alert>
                </Snackbar>
        </>
    )
}
export default CartContent;
