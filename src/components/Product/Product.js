import React, { useState, useRef, useEffect} from 'react';
import Slider from 'react-slick';
import { getPopularProduct } from "../../actions/getProductAction";
import { useDispatch, useSelector } from "react-redux";
import ProductCard from '../ProductCard/ProductCard';
function Product() {
    const dispatch = useDispatch();
    const { products } = useSelector((reduxData) => reduxData.getProductReducer);
    useEffect(() => {
        dispatch(getPopularProduct());
        console.log("products length", products)
    }, []);
    const ref = useRef({});

    const next = () => {
      ref.current.slickNext();
    };
  
    const previous = () => {
      ref.current.slickPrev();
    };
    const settings = {
        className: 'section-outstanding__slider',
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        speed: 1000,
        responsive: [
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
              }
            },
            {
                breakpoint: 576,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  infinite: true,
                }
              },
          ]
      };

    return (
        <>
            <section className="new_arrivals_area section-padding-80 clearfix">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="section-heading text-center">
                                <h2>Popular Products</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {/* <div className="popular-products-slides owl-carousel" style={{display: "flex"}}> */}
                            <Slider ref={ref} {...settings} className = "home-slide">
                            {products.map((product, index) => (
                                        <ProductCard key={index} product={product} />
                                    ))}
                      </Slider>
                            {/* </div> */}
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default Product;
