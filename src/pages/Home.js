import React from 'react'
import Header from '../components/Header/Header'
import TopCategory from '../components/TopCategory/TopCategory'
import Product from '../components/Product/Product'
import Brand from '../components/Brand/Brand'
import Footer from '../components/Footer/Footer'
import WelcomeArea from '../components/WelcomeArea/WelcomeArea'


function Home() {
    return (
        <>
            <Header />
            <WelcomeArea />
            <TopCategory />

            <Product />
            <Brand />
            <Footer />
        </>
    )
}

export default Home;
