import React from 'react'
import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import DetailProduct from '../components/DetailProduct/DetailProduct'
import BreadCrumb from '../components/BreadCrumb/BreadCrumb';
import { useSelector } from "react-redux";

function ProductInfo() {
    const {productInfo} = useSelector(reduxData => reduxData.getProductReducer);
    console.log("product", productInfo);
    const breadcrumbArray = [
        {
          name: "Home",
          url: "/",
        },
        {
          name: "All Product",
          url: "/products",
        },
      ];
    
    return (
        <>
          

            <Header />
            <BreadCrumb breadcrumbArray={breadcrumbArray} pageName={productInfo.name} />
            <DetailProduct />
            <Footer />
        </>
    )
}

export default ProductInfo;
