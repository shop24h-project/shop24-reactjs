import React from 'react'
import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import ProductList from '../components/ProductList/ProductList'
import BreadCrumbCategory from '../components/BreadCrumbCategory/BreadCrumbCategory'
import { useParams } from "react-router-dom";

function Category() {
    const { name } = useParams();
    return (
        <>
            <Header/>
            <BreadCrumbCategory name={name}/>
            <ProductList name={name}/>
            <Footer />
        </>
    )
}

export default Category;