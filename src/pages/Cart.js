import React from 'react'
import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import CartContent from '../components/CartContent/CartContent'
function Cart() {
    return (
        <>
            <Header/>
            <CartContent/>
            <Footer />
        </>
    )
}

export default Cart;
