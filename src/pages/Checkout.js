import React from 'react'
import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import CheckoutContent from '../components/CheckoutContent/CheckoutContent'
function Cart() {
    return (
        <>
            <Header/>
            <CheckoutContent/>
            {/* <Footer /> */}
        </>
    )
}

export default Cart;
